@extends('layouts.app')

@section('content')
<div>
<div class="row">
    <div class="col-md-6">
        <canvas id="vulnerability-chart"></canvas>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div id="attack-map"></div>
    </div>
</div>
</div>

@endsection

@section('scripts')
<script>

    const labels = [
        'Threatent',
        'Alertness',
    ];

    const data = {
        labels: labels,
        datasets: [{
            label: {!!json_encode($report['alert']) !!},
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [{!!json_encode($report['Threatent']) !!}, {!!json_encode($report['Alertness']) !!}],
        }]
    };

    const config = {
        type: 'bar',
        data: data,
        options: {}
    };

    new Chart(
        document.getElementById('vulnerability-chart').getContext('2d'),
        config
    );
    
    let map = L.map('attack-map', {
        center: [51.505, -0.09],
        zoom: 13
    })
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1
    }).addTo(map)
</script>
@endsection