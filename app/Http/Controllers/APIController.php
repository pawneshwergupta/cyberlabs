<?php

namespace App\Http\Controllers;

class APIController extends Controller {
    public function getData() {
        $data = file_get_contents(public_path('response.json'));
        $data = json_decode($data, true);

        return response()->json([
            'status' => '1',
            'data' => $data
        ], 200);
      
    }
}