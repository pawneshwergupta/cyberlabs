<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{
  public function index()
  {
    $data = file_get_contents(public_path('response.json'));
    $data = json_decode($data, true);

    $report = $data['Report'][0];
    $threatend = $data['Threatend'][0];
    
    return view('welcome', compact('report', 'threatend'));
  }
}
